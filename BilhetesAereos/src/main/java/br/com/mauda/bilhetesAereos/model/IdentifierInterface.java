package br.com.mauda.bilhetesAereos.model;

public interface IdentifierInterface {
	public Long getId();
	public void setId(Long id);
}
